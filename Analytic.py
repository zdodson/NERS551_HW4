"""Analytic comparison for a single and dual reactivity slope

"""

import numpy as np

def singleslope(lam, beta, p0, t, rdot, rho0):
    """Find an analytic power solution for the single reactivity ramp problem 
    in the PJA approximation. 
    
    lam = Single group decay constant
    beta = Effective delayed neutron fraction
    p0 = Initial power level
    t = Times at which to calculate. Some sort of array/vector.
    rdot = Reactivity increase rate. Assumed non-zero.
    rho0 = Initial reactivity value.
    
    """
    
    c = 1 + (lam * beta / rdot)
    tau = (beta - rho0) / rdot
    p00 = beta * p0 / (beta - rho0)
    #p = [p00 * ((tau / (tau - ti + t[0])) ** c) * exp(-lam * (ti-t[0])) for ti in t]
    return p00 * ((tau / (tau - t + t[0])) ** c) * np.exp(-lam * (t-t[0]))

def doubleslope(lam, beta, p0, t, tmax, rdot1, rdot2, rho0):
    """Find an analytic power solution for the  asymmetric reactivity ramp problem 
    in the PJA approximation. 
    
    lam = Single group decay constant
    beta = Effective delayed neutron fraction
    p0 = Initial power level
    t = Times at which to calculate. Some sort of array/vector.
    tmax = The time at which the reactivity ramp reverses.
    rdot (1 and 2) = Reactivity increase rate. Assumed non-zero.
    rho0 = Initial reactivity value.
    
    """
    
    c1 = 1 + (lam * beta / rdot1)
    c2 = 1 + (lam * beta / rdot2)
    tau1 = (beta - rho0) / rdot1
    tau2 = (beta - (rho0 + rdot1*tmax)) / rdot2
    t1 = t[t < tmax]
    t2 = t[t >= tmax]
    p00 = beta * p0 / (beta - rho0)
    p1 = p00 * ((tau1 / (tau1 - t1)) ** c1) * np.exp(-lam * t1)
    pmax = p00 * ((tau1 / (tau1 - tmax) )** c1) * np.exp(-lam * tmax)
    p2 = pmax * ((tau2 / (tau2 - t2 + tmax)) ** c2) * np.exp(-lam * (t2-tmax))
    return np.concatenate((p1, p2))

if __name__ == "__main__":
    from argparse import ArgumentParser as AP
    from HW4 import ExactP, read_input, makeplot
    
    parser = AP(description = "Tests the ramp analytic solutions on the " + 
                "exact point kinetics model")
    parser.add_argument('i', help="Input yml file")
    parser.add_argument('-tc', type=float, help="Time at which to change ramp direction")
    parser.add_argument('-lambmx', action="store_true", help="Flag to do all lambdas")
    parser.add_argument('-p', help="EPS file to store the solution in")
    
    args = parser.parse_args()
    
    lam, beta, time, gam_d, theta, f_fp, lam_H, eta, rhoin, p0, Lam = read_input(args.i)
    jump = rhoin[0]
    ramp1 =  (rhoin[1]-rhoin[0]) / (time[1] - time[0])
    lambinv = np.sum(beta[0,:]) / np.sum(beta[0,:] / lam)
    lambdir = np.sum(beta[0,:] * lam) / np.sum(beta[0,:])
    betaeff = np.sum(beta[0,:])
    
    t, p, zeta, rhos, H = ExactP(args.i)
    
    rho = rhoin
    imax = np.argmax(p)
    tmax = t[imax]
    rdot = (rho[imax] - rho[imax - 1]) / (tmax - t[imax - 1])
    lamb810 = - rdot / rho[imax]
    
    t1 = t[t<args.tc] if args.tc else t
    psingle = singleslope(lambinv, betaeff, p0, t1, ramp1, jump)
    if args.tc:
        rho2 = rhoin[t>args.tc]
        t2 = t[t>args.tc]
        ramp2 = (rho2[1] - rho2[0]) / (t2[1] - t2[0])
        pdouble1 = doubleslope(lambinv, betaeff, p0, t, args.tc, ramp1, ramp2, jump)
        relerr1 = 100*(pdouble1 / p - 1)
        pdouble2 = doubleslope(lambdir, betaeff, p0, t, args.tc, ramp1, ramp2, jump)
        relerr2 = 100*(pdouble2 / p - 1)
        pdouble3 = doubleslope(lamb810, betaeff, p0, t, args.tc, ramp1, ramp2, jump)
        relerr3 = 100*(pdouble3 / p - 1)
        makeplot(t, [relerr1, relerr2, relerr3] if args.lambmx else [relerr1], t_max = np.max(t),
                     ylabel="Relative Error [%]", 
                     view = False if args.p else True,
                     labellist = ["Inverse Weight", 'Direct weight', 'Eq. 8.10'] if args.lambmx else None,
                     output = args.p)
    else:
        relerr = 100*(psingle / p - 1)
        print('dt = ' + str((t[1] - t[0])) + ' err_at_tmax = ' + str(abs(relerr[-1])))
        makeplot(t, [relerr], t_max = np.max(t), 
                     ylabel="Relative Error [%]", 
                     view = False if args.p else True,
                     output = args.p)
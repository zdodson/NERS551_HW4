"""
Created on Wed Mar  8 18:20:51 2017

"""

if __name__ == '__main__':
    from argparse import ArgumentParser as AP
    from HW4 import ExactP, makeplot
    from yaml import dump as ydump
    import numpy as np
    from os.path import splitext, join
    
    parser = AP(description = "Tests the PARCS given input")
    parser.add_argument('res', help="PARCS result file")
    parser.add_argument('p', nargs = 2, help="EPS files to store the solution in")
    parser.add_argument('-pp', action="store_true", help="Flag to view plots")
    parser.add_argument('-rres', help="PARCS reactivitywith file")
    parser.add_argument('-o', help="Optional output file, for post processing")
    
    args = parser.parse_args()
    
    with open(args.res, 'r') as resf:
        lines = resf.readlines()[2:]
    result = np.array([float(line.split()[-1]) for line in lines])
    reacres = np.array([float(line.split()[1]) * 0.0076 for line in lines])
    
    powertpl, extp = splitext(args.p[0])
    reactpl, extr = splitext(args.p[1])
    
    for i in [9]:
        for j in [2]:
                    
            t, p, zeta, rhos, H = ExactP('thermo_102.yml')
            if len(result) != len(t):
                raise IndexError("You should have the results at the same timescale as the input!")
                
            makeplot(t, [result, H], t_max = 10.0, view = args.pp,
                     ylabel = "Power [Normalized]",
                     output = powertpl + '102' + extp,
                     labellist=["PARCS", "EPK"])
                
            makeplot(t, [100 * (H / result - 1)], t_max = 10.0, view = args.pp,
                     ylabel = "Relative Error [%]",
                     output = powertpl + '102err' + extp)
            makeplot(t, [reacres, rhos[1]], t_max = 10.0, view = args.pp,
                     ylabel = "Reactivity [1]",
                     output = reactpl + '102' + extp,
                     labellist=["PARCS", "EPK"])
    
    if args.o:
        with open(args.o, 'w') as of:
            for v in zip(t, p):
                of.write(','.join(v) + '\n')
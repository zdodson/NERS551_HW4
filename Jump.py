"""
Created on Mon Mar  6 21:27:00 2017

"""

import numpy as np

def JumpAnalytic(beta, lam, rho, p0, t):
    """Analytic solution to the reactivity subprompt supercritical jump.
    
    beta = Delayed neutron effective fraction
    lam = One group delayed neutrons decay constant, in 1/sec.
    rho = Reactivity jump value
    p0 = Initial power
    t = Times to calculate at, in sec.
    
    """
    
    coeff = lam * rho / (beta-rho)
    p00 = p0 * beta / (beta-rho)
    return p00*np.exp(coeff * t)

if __name__ == "__main__":
    from argparse import ArgumentParser as AP
    from HW4 import ExactP, read_input, makeplot
    
    parser = AP(description = "Tests the jump analytic solutions on the " + 
                "exact point kinetics model")
    parser.add_argument('i', help="Input yml file")
    parser.add_argument('-o', help="Optional output file, for post processing")
    parser.add_argument('-p', help="EPS file to store the solution in")
    
    args = parser.parse_args()
    
    lam, beta, time, gam_d, theta, f_fp, lam_H, eta, rhoin, p0, Lam = read_input(args.i)
    jump = rhoin[0]
    
    lamb = np.sum(beta[0,:]) / np.sum(beta[0,:] / lam)
    betaeff = np.sum(beta[0,:])
    
    t, p, zeta, rhos, H = ExactP(args.i)
    
    ansol = JumpAnalytic(betaeff, lamb, jump, p0, t)
    makeplot(t, [100*(p / ansol - 1)], t_min = 600*Lam[0], t_max = np.max(t),
                     ylabel="Relative Error [%]", 
                     view = False if args.p else True,
                     output = args.p)
    
    
    if args.o:
        with open(args.o, 'w') as of:
            for v in zip(t, p, ansol):
                of.write(','.join(v) + '\n')
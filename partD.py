"""
Created on Wed Mar  8 18:20:51 2017

"""

if __name__ == '__main__':
    from argparse import ArgumentParser as AP
    from HW4 import ExactP, makeplot
    import numpy as np
    
    parser = AP(description = "Tests the PARCS given input")
    parser.add_argument('i', help="Input yml file")
    parser.add_argument('res', help="PARCS result file")
    parser.add_argument('p', nargs = 2, help="EPS files to store the solution in")
    parser.add_argument('-pp', action="store_true", help="Flag to view plots")
    parser.add_argument('-o', help="Optional output file, for post processing")
    
    args = parser.parse_args()
            
    t, p, zeta, rhos, H = ExactP(args.i)
    
    with open(args.res, 'r') as resf:
        lines = resf.readlines()[2:]
    result = np.transpose(np.array([float(line.split()[-1]) for line in lines]))
    resreac = np.array([float(line.split()[1]) * 0.0076 for line in lines])
    if len(result) != len(t) or len(resreac) != len(t):
        raise IndexError("You should have the results at the same timescale as the input!")
        
    makeplot(t, [result, H], t_max = 2.0, view = args.pp,
             ylabel = "Power [Normalized]",
                     output = args.p[0], 
                     labellist=["PARCS", "EPK"])
    makeplot(t, [resreac, rhos[1]], t_max = 2.0, view = args.pp,
             ylabel = "Reactivity [1]",
                     output = args.p[1],
                     labellist=["PARCS", "EPK"])
    
    if args.o:
        with open(args.o, 'w') as of:
            for v in zip(t, p):
                of.write(','.join(v) + '\n')
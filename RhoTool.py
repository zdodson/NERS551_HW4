#!/usr/bin/env python

import numpy as np
import os,subprocess
import matplotlib.pyplot as plt

if __name__ == "__main__":
  if os.path.isfile('err.txt'): os.remove('err.txt')
  if os.path.isfile('err2.txt'): os.remove('err2.txt')
  
  i = 'c_1_b.yaml'
  # i = 'doubleramp2.yml'
  a1 = 0.0038
  thetas = [0.5,1.0]
  tnums = [25]
  for j in range(7):
    item = (tnums[-1] * 2)
    tnums.append(item)
  
  tend = 5.0
  
  text = []
  
  with open(i,"r") as f:
    for line in f:
      text.append(line)
  
  for theta in thetas:
    for k,tnum in enumerate(tnums):
      dt = tend / tnum
      dt = 0.01
      tnum = tend / dt
      t = np.linspace(0,tend,tnum)
      
      lrho1 = len(t[t <= 1.0])+1
      lrho2 = len(t[t > 1.0])
      rho1 = np.linspace(0,a1,lrho1)
      rho2 = np.linspace(a1,0,lrho2)
      
      rho = np.concatenate((rho1,rho2[1:]))
      # rho = np.linspace(0,a1,tnum)
      tind = np.where(rho == max(rho1))[0][0]
      tmax = t[tind]
      l = len(t)
      if len(t) > l:
        rho = np.append( rho, 0 )
      
      name = 'temp_' + str(k) + '_' + str(theta) + '.yml'
      with open(name,'w') as f:
        for line in text:
          if '{X}' in line: line = line.replace('{X}',str('%f' % theta))
          f.write(line)
        f.write("\nrho_im : [")
        for i,value in enumerate(rho):
          f.write(str(value))
          if i < len(rho) - 1:
            f.write(", ")
          if i % 5 == 0 and i > 1:
            f.write("\n  ")
        f.write(" ]")
        f.write("\ntime : [")
        for i,value in enumerate(t):
          f.write(str(value))
          if i < len(t) - 1:
            f.write(", ")
          if i % 5 == 0 and i > 1:
            f.write("\n  ")
        f.write(" ]")
      quit()
      
      errname = 'err.txt' if theta == 0.5 else 'err2.txt'
      pltname = 'plt_' + str(k) + '_' + str(theta) + '.eps'
      # subprocess.call('python -W ignore Analytic.py -p ' + pltname + ' ' + name + ' >> ' + errname,shell=True)
      subprocess.call('python -W ignore Analytic.py -p ' + pltname + ' -tc ' + str(tmax) + ' -tind ' + str(tind) + ' ' + name + ' >> ' + errname,shell=True)
      os.remove(name)
      
    if theta == 0.5:
      with open('err.txt') as f:
        dt_v,err = [],[]
        for line in f:
          line = line.split()
          dt_v.append(float(line[2]))
          err.append(float(line[5]))
        # print(dt_v)
        # print(err)
    else:
      with open('err2.txt') as f:
        dt_v2,err2 = [],[]
        for line in f:
          line = line.split()
          dt_v2.append(float(line[2]))
          err2.append(float(line[5]))
        # print(dt_v2)
        # print(err2)

  # for i in range(len(err)):
  #   if i > 0:
  #     print(err[i] / err[i - 1])

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(dt_v,err,"ko",label="Crank-Nicholson")
ax.plot(dt_v2,err2,"ro",label="Implicit")
ax.legend(loc='best',shadow=True)
ax.set_title("Relative Error as a Function of Time-step")
ax.set_xlabel("Time-step (s)")
ax.set_ylabel('Relative Error (%)')
ax.grid(True)
ax.set_xscale("log")
ax.set_yscale("log")
fig.savefig('err.eps')
plt.close()

# os.remove('err.txt')
# os.remove('err2.txt')
# os.remove('plt.eps')

  # fig = plt.figure()
  # ax = fig.add_subplot(111)
  # ax.plot(t,rho)
  # ax.set_title("Imposed Reactivity as a Function of Time")
  # ax.set_xlabel("Time (s)")
  # ax.set_ylabel('Reactivity')
  # ax.grid(True)
  # fig.savefig('rho_' + str(k) + '.eps')
  # plt.close()



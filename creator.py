"""
Created on Thu Mar  9 13:55:52 2017

"""

from HW4 import read_input as readin

lam, beta, time, gam_d, theta, f_fp, lam_H, eta, rhoin, p0, Lam = readin('thermo.yml')
gamran = [0.1 * i * gam_d for i in range(1,21)]
lamHs = [0.1 * i * lam_H for i in range(1,21)]

with open('thermo.yml', 'r') as inpf:
    inlines = inpf.readlines()
for i in range(20):
    for j in range(20):
        fn = 'thermo_%d_%d.yml' % (i,j)
        with open(fn, 'w') as f:
            for line in inlines:
                if line.split() and 'gamma_d' == line.split()[0]:
                    f.write('gamma_d : %f \n' % gamran[i])
                elif line.split() and 'lambda_h' == line.split()[0]:
                    f.write('lambda_h : %f \n' % lamHs[j])
                else:
                    f.write(line)
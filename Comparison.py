if __name__ == "__main__":
  from HW4 import ExactP, makeplot, beta_weighted_lambda, inverse_weighted_lambda
  import numpy as np
  
  t_1, p_1, zeta_1, rhos_1, H_1 = ExactP('c_2_1g.yml')
  t_6, p_6, zeta_6, rhos_6, H_6 = ExactP('c_2_6g.yml')
  
  p_diff = 100*(p_6 / p_1 - 1)
  
  p = [p_1,p_6]
  
  l = len(zeta_6)
  zeta = np.zeros([l])
  zeta_1_sum = np.zeros([l])
  for i in range(l):
    zeta[i] = sum(zeta_6[i,:])
    zeta_1_sum[i] = sum(zeta_1[i,:])
  
  zeta_diff = 100*(zeta / zeta_1_sum - 1)
  
  makeplot(t_1, [p_diff], t_max = np.max(t_1),
               ylabel="Relative Power Error [%]", 
               view = False,
               output = 'c_2_err.eps')
  makeplot(t_1, p, t_max = np.max(t_1),
               ylabel="Power", 
               view = False,
               output = 'c_2_pwr.eps',
               labellist=['One Group Power','Six Group Power'])
  makeplot(t_1, [zeta_1_sum], t_max = np.max(t_1),
               ylabel="Relative Precursor Concentration Error (%)", 
               view = False,
               output = 'c_2_pre.eps')

  lam = np.array([0.0128, 0.0318, 0.119, 0.3181, 1.4027, 3.9286])
  beta = np.array([0.0002584, 0.00152, 0.0013908, 0.0030704, 0.001102, 0.0002584])
  
  print(beta_weighted_lambda(lam, beta))
  print(inverse_weighted_lambda(lam, beta))
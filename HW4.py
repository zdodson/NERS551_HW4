#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

def read_input(filepath):
    """Reads the required input from a YAML input file. Returns a list of
    constants and initial conditions
    
    filepath = File input path
    
    """
    from yaml import load as yload
    
    params = ["lambda", "beta", "time", "gamma_d", "theta", "FFP", "lambda_h", 
              "eta", "rho_im", "p0", "Lambda"]
    
    with open(filepath, 'r') as inpf:
        prsd = yload(inpf)
    
    tnum = int(prsd["tmax"] / prsd["dt"]) if "time" not in prsd else len(prsd["time"])
    if 'time' not in prsd:
        prsd["time"] = np.linspace(0,prsd["tmax"],tnum)
    else:
        prsd["time"] = np.ones(tnum) * prsd["time"]
        
    prsd["lambda"] = np.ones(6) * prsd["lambda"]
    
    try:
        beta = float(prsd["beta"])
        prsd["beta"] = np.ones([tnum, 6]) * prsd["beta"] / 6 #Total beta given
    except TypeError: #Beta is not a single value.
        #Beta is either an array or a matrix
        prsd["beta"] = np.ones([tnum, 6]) * prsd["beta"]
        
    if 'dollar' in prsd:
        prsd["rho_im"] = np.sum(prsd["beta"], axis=1) * prsd["rho_im"]
    else:
        prsd["rho_im"] = np.ones(tnum) * prsd["rho_im"]
        
    prsd["Lambda"] = np.ones(tnum) * prsd["Lambda"]
    prsd["FFP"] = np.ones(tnum) * prsd["FFP"]
    return [prsd[param] for param in params]

# Eqn. 10
def expint( lam, dt_n ):
  return np.exp( lam * dt_n )

# Eqn. 11
def kappa_0( dt_n, lam ):
    if np.all(np.absolute(lam * dt_n) < 1e-9):
        return dt_n * expint( lam, dt_n )
    else:
        return (expint(lam, dt_n) - 1.0) / lam

# Eqn. 12
def kappa_1( dt_n, lam ):
    if np.all(np.absolute(lam * dt_n) < 1e-5):
        return ( dt_n ** 2 ) * expint( lam, dt_n ) / 2
    else:
        return (dt_n * expint(lam, dt_n) - kappa_0(dt_n, lam)) / lam

# Eqn. 13
def kappa_2( dt_n, lam ):
    if np.all(np.absolute(lam * dt_n) < 2e-4):
        return ( dt_n ** 3 ) * expint( lam, dt_n ) / 3
    else:
        return ((dt_n ** 2) * expint(lam, dt_n) - 2 * kappa_1( dt_n, lam)) / lam

# Eqn. 14
def zeta( p_n, omega_n, zetahat_n ):
  return p_n * omega_n + zetahat_n

# Eqn. 15
def omega( w_kn, Lam_0, beta_kn, Lam_n, lam, gam, dt_n ):
  return (w_kn * Lam_0 * ( beta_kn / Lam_n ) * 
          ( ( kappa_2( dt_n, lam) + gam * dt_n * kappa_1( dt_n, lam ) ) ) / 
          ( ( 1 + gam ) * (dt_n ** 2) ) )

# Eqn. 16
def zetahat( w_kn, zeta_knm1, Lam_0, p_nm1, p_nm2, beta_knm1, beta_knm2, 
            Lam_nm1, Lam_nm2, lam, gam, dt_n ):
  return (w_kn * zeta_knm1 + 
          w_kn * Lam_0 * p_nm1 * ( beta_knm1 / Lam_nm1) * 
                ( kappa_0( dt_n, lam ) - 
                 ((kappa_2(dt_n, lam) + (gam - 1) * dt_n * kappa_1(dt_n, lam)) / 
                  ( gam * (dt_n ** 2) ))) +
          w_kn * Lam_0 * p_nm2 * ( beta_knm2 / Lam_nm2) *
                (kappa_2(dt_n, lam) - dt_n * kappa_1(dt_n, lam)) / 
                (gam * (1 + gam) * (dt_n ** 2))
          )

# Eqn. 17
def w_func( lam, dt_n ):
  return 1 / ( expint( lam, dt_n ) )

# Eqns. 19
def tau( lam_n_vector, omega_n_vector ):
  return sum( lam_n_vector * omega_n_vector )

def S_d_hat( lam_n_vector, zetahat_n_vector ):
  return sum( lam_n_vector * zetahat_n_vector )

def S_d_nm1( lam_nm1_vector, zeta_nm1_vector ):
  return sum( lam_nm1_vector * zeta_nm1_vector )
  
# Eqn. 22
def H( f_fp, p_n ):
  return f_fp * p_n

# Eqn. 23
def alpha( dt_nm1, p_nm1, p_nm2 ):
  return ( 1 / dt_nm1 ) * np.log( p_nm1 / p_nm2 )

# Eqn. 24
def check( p_n, alpha_n, dt_n, p_nm1, p_nm2, gam ):
  if ( p_n - expint( alpha_n, dt_n ) * p_nm1 ) <= ( p_n - p_nm1 - ( p_nm1 - p_nm2 ) / gam ):
    return False
  else:
    return True

# Eqn. 31
def rho( a_1_value, p_n, b_1_value ):
  return a_1_value * p_n + b_1_value

# Eqn. 32
def a_1( gam_d, f_fpn, lam_H, gam, dt_n ):
  return (( gam_d * f_fpn / ( expint( lam_H, dt_n ) ) ) * 
           ( kappa_2( dt_n, lam_H ) + kappa_1( dt_n, lam_H ) * gam * dt_n ) / 
           ( ( 1 + gam ) * (dt_n ** 2) ) )

# Eqn. 33
def b_1( rho_imn, rho_d_nm1, gam_d, eta, ps0, lam_H, H_nm1, H_nm2, gam, dt_n ):
  term1 = rho_imn
  term2 = (( rho_d_nm1 - gam_d * eta * ps0 * kappa_0( dt_n, lam_H ) ) / 
          ( expint( lam_H, dt_n ) ))
  term3 = ( gam_d / expint( lam_H, dt_n ) )
  term4 = (H_nm1 * ( kappa_0( dt_n, lam_H ) - 
                    ( ( kappa_2(dt_n, lam_H) + (gam - 1) * dt_n * kappa_1(dt_n, lam_H) ) / 
                      ( gam * (dt_n ** 2) ) ) ) )

  term5 = H_nm2 * ( ( kappa_2( dt_n, lam_H ) - kappa_1( dt_n, lam_H ) * dt_n) / 
                  ( ( 1 + gam ) * gam * (dt_n ** 2) ) )
  return term1 + term2 + term3 * ( term4 + term5 )

# Eqn. 36
def a( theta, dt_n, as1, Lam_n ):
  return theta * dt_n * as1 / Lam_n

# Eqn. 37
def b( theta, dt_n, bs1, beta_n, Lam_n, alpha_n, tau_n, Lam_0 ):
  return theta * dt_n * ( ( ( ( bs1 - beta_n ) / Lam_n ) - alpha_n ) + ( tau_n / Lam_0 ) ) - 1

# Eqn. 38
def c( theta, dt_n, Lam_0, S_d_hat_n, alpha_n, rho_nm1, beta_nm1, Lam_nm1, S_d_nm1, p_nm1 ):
  term1 = theta * dt_n * S_d_hat_n / Lam_0
  term2 = expint( alpha_n, dt_n )
  term3 = (( 1 - theta ) * dt_n * 
           ( ( (( rho_nm1 - beta_nm1 ) / Lam_nm1) - alpha_n ) * p_nm1 + 
            ( S_d_nm1 / Lam_0 ) ) + p_nm1 )
  return term1 + term2 * term3

# Eqn. 39-40
def flux_level( a_value, b_value, c_value ):
  if a_value < 0:
    return ( -b_value - np.sqrt( b_value * b_value - 4 * a_value * c_value ) ) / ( 2 * a_value )
  else:
    return -1 * c_value / b_value


def makeplot(t, vs, t_min = 0.0, t_max = None, y_min = None, y_max = None,
             xlabel="Time (s)", ylabel="Power [normalized]", 
             view=True, output=None, grid=True, labellist=None):
  """Plot a a set of vectors on a graph.
  
  t = X-axis vector
  vs = A list of Y-axis vectors
  t_max = Maximal value to plot at
  xlabel = Label for X axis
  ylabel = Label for Y Axis
  view = Flag for whether to display results or not.
  output = A filepath to where to save the figure.

  """
  
  fig = plt.figure()
  ax = fig.add_subplot(111)
  if not t_max:
      t_max = np.max(t)
  from numpy import logical_and as land
  
  for i,v in enumerate(vs):
    ax.plot(t[land(t>t_min, t<t_max)], v[land(t>t_min, t<t_max)],
              label=labellist[i] if labellist else None)
  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)
  ax.grid(True)
  ax.set_xlim([t_min,t_max])
  if labellist: ax.legend(loc='best',shadow=True)
  
  if y_min or y_max:
      y_min = min([np.min(vvs[t>t_min and t<t_max]) for vvs in vs]) if not y_min else y_min
      y_max = max([np.max(vvs[t>t_min and t<t_max]) for vvs in vs]) if not y_max else y_max
      ax.set_ylim([y_min, y_max])
  
  if view:
      plt.show()
  if output:
      plt.savefig(output)
          
  plt.close()
  
def beta_weighted_lambda( lam, beta ):
  return sum( lam * beta ) / sum( beta )

def inverse_weighted_lambda( lam, beta ):
  return sum( beta ) / sum( beta / lam )
  
def ExactP(filepath):
    """Calculates the result of the exact point kinetics using values from an 
    input file. Assumes 6 decay groups. 1 decay group can be implemented by
    setting a constant vector lambda and a constant vector beta such that its
    sum is the total delayed fraction.
    
    filepath = Input file path
    
    Output:
    Outputs a tuple of:
        time, power, precursors, (imposed reactivity, actual reactivity), H
    
    """
    
    lam, beta, t, gam_d, theta, f_fp, lam_H, eta, rho_im, p0, Lam = read_input(filepath)
    t_num = len(t)
    delt = t[1:] - t[:-1]
    gam       = np.insert(delt[:-1] / delt[1:], 0, np.ones(2))
    zeta_v    = np.zeros([t_num,6])
    zetahat_v = np.zeros([t_num,6])
    w         = np.zeros([t_num,6])
    S_d_v     = np.zeros(t_num)
    S_d_hat_v = np.zeros(t_num)
    p         = np.zeros(t_num)
    p[0] = p0
    H_v       = np.zeros(t_num)
    rho_v     = np.zeros(t_num)
    rho_d     = np.zeros(t_num)
    zeta_v[0,:] = ( beta[0,:] / lam ) * p[0] # Initial zeta values
    for j,time in enumerate(t[1:]):
        i = j+1
        dt_n = delt[j]
        # Step 1: Prepare omega_n and zetahat_kn with Eqns. 15, 16, and 17.
        w[i,:] = w_func( lam, dt_n )
        omega_n = omega( w[i,:], Lam[0], beta[i,:], Lam[i], lam, gam[i], dt_n )
        zetahat_v[i,:] = zetahat( w[i,:], zeta_v[max([0,i-1]),:], Lam[0], 
                                  p[max([0,i-1])], p[max(0,i-2)], 
                                  beta[max([0,i-1]),:], beta[max([0,i-2]),:], 
                                  Lam[max([0,i-1])], Lam[max([0,i-2])], 
                                  lam, gam[i], dt_n )

        # Step 2: Prepare tau_n, S_d_hat_n, and S_d_hat_nm1
        tau_n = tau( lam, omega_n )
        S_d_v[max([0,i-1])] = S_d_nm1( lam, zeta_v[max([0,i-1]),:] )    
        S_d_hat_v[i] = S_d_hat( lam, zetahat_v[i,:] )

        # Step 3: Prepare a_1, b_1
        a_1_n = a_1( gam_d, f_fp[i], lam_H, gam[i], dt_n )    
        b_1_n = b_1( rho_im[i], rho_d[i-1] if i>=1 else 0, gam_d, eta, 
                    p[0], lam_H, H_v[i-1] if i>=1 else 1, 
                    H_v[i-2] if i>= 2 else 1, gam[i], dt_n )

        # Step 4: Determine the transformation parameter
        alpha_n = alpha( delt[max([j-1,0])], p[max([0,i-1])], p[max([0,i-2])] )

        # Step 5: Prepare a, b, and c
        a_n = a( theta, dt_n, a_1_n, Lam[i] )

        b_n = b(theta, dt_n, b_1_n, sum(beta[i,:]), Lam[i], alpha_n, 
                tau_n, Lam[0])    
        c_n = c( theta, dt_n, Lam[0], S_d_hat_v[i], alpha_n, 
                rho_v[i-1] if i>=1 else 0, sum(beta[max([0,i-1]),:]), 
                Lam[max([0,i-1])], S_d_v[max([0,i-1])], p[max([0,i-1])] )

        # Step 6: Evaluate flux level
        p[i] = flux_level( a_n, b_n, c_n )

        # Step 7: Check if exponential transformation is acceptable with Eqn. 24, if not, redo step 6 and 7
        if check( p[i], alpha_n, dt_n, p[max([0,i-1])], p[max([0,i-2])], gam[i] ):
            b_n = b( theta, dt_n, b_1_n, sum(beta[i,:]), Lam[i], 0, tau_n, 
                    Lam[0] )
            c_n = c(theta, dt_n, Lam[0], S_d_hat_v[i], 0, 
                    rho_v[i-1] if i>=1 else 0, sum(beta[max([0,i-1]),:]), 
                    Lam[max([0,i-1])], S_d_v[max([0,i-1])], p[max([0,i-1])])
            
            p[i] = flux_level( a_n, b_n, c_n )

        # Step 8: Evaluate power level
        H_v[i] = H( f_fp[i], p[i] )
        zeta_v[i,:] = zeta( p[i], omega_n, zetahat_v[i,:] )
        rho_v[i] = rho( a_1_n, p[i], b_1_n )
        rho_d[i] = rho_v[i] - rho_im[i]
        
    return (t, p, zeta_v, (rho_im, rho_v), H_v)


if __name__ == "__main__":
  from argparse import ArgumentParser as AP
  from pickle import dump
  parser = AP(description = "Exact Point Kinetics Solver")
  parser.add_argument("i", help = "Input file to use")
  parser.add_argument("-o", help = "Output file to use")
  parser.add_argument('-p', help= "Plot file to write to.")
  parser.add_argument('-pr', help= "Plot file to write to for reactivity.")
  parser.add_argument('-pp', help= "Plot file to write to for precursors.")
  
  args = parser.parse_args()
  
  t, p, zetav, rhos, Hv = ExactP(args.i)
  rho_im, rhov = rhos
  
  if args.o:
      with open(args.o, 'wb') as of:
          dump({"t":t, "p":p, "zeta":zetav, "rho_im":rho_im, "rho":rhov, 
                "H":Hv}, of)
    
  makeplot(t, [p], output=args.p, view = False if args.p else True)
  makeplot(t, [rhov], ylabel="Reactivity", output=args.pr, view = False if args.pr else True)
  makeplot(t, [zetav], ylabel="Precursor Concentration (relative to power)", output=args.pp, view = False if args.pp else True)
  
  